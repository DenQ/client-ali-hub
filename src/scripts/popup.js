import ext from "./utils/ext";
import truncate from "./utils/truncate";
import storage from "./utils/storage";

var popup = document.getElementById("app");

var template = (res) => {
  const list = res.list.map((item) => {
    return `
      <div title="${item.name}">
        <span class="action-remove" title="remove" data-remove-id=${item.id}>x</span>
        id: ${item.id} - <a href="${item.url}" target="_blank">${ truncate(item.name, 10)}</a>
      </div>
    `;
  }).join('');
  var json = JSON.stringify(res.data);
  return `
    <div class="list-container">${list}</div>
    <div class="action-container">
      <button data-bookmark='${json}' id="save-btn" class="btn btn-primary">Save page</button>
    </div>
  `;

}
var renderMessage = (message) => {
  var displayContainer = document.getElementById("display-container");
  displayContainer.innerHTML = `<p class='message'>${message}</p>`;
}

var renderBookmark = (data) => {
  var displayContainer = document.getElementById("display-container")
  if(data) {
    fetchList()
      .then((results) => {
        return results.json();
      })
      .then((results) => {
        var tmpl = template({ list:results, data });
        displayContainer.innerHTML = tmpl;
      })

  } else {
    renderMessage("Sorry, could not extract this page's title and URL")
  }
}

function fetchList() {
  return fetch('http://localhost:4040/api/products', {
    mode: 'basic',
    credentials: 'include',
  });
}

ext.tabs.query({active: true, currentWindow: true}, function(tabs) {
  var activeTab = tabs[0];
  chrome.tabs.sendMessage(activeTab.id, { action: 'process-page' }, renderBookmark);
});

popup.addEventListener("click", function(e) {
  if(e.target && e.target.matches("#save-btn")) {
    e.preventDefault();
    var data = e.target.getAttribute("data-bookmark");
    ext.runtime.sendMessage({ action: "perform-save", data: data }, performSave);
  }
  if (e.target && e.target.matches('[data-remove-id]')) {
    e.preventDefault();
    const productId = e.target.getAttribute("data-remove-id");
    ext.runtime.sendMessage({ action: "perform-remove", data: { productId} }, performRemove);
  }
});

function performSave(response) {
  if(response && response.action === "saved") {
    renderMessage("Your bookmark was saved successfully!");
  } else {
    renderMessage("Sorry, there was an error while saving your bookmark.");
  }
}

function performRemove(response) {
  renderMessage("Removed product!");
}
