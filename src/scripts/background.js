import ext from "./utils/ext";

function pushProduct(data) {
  data = JSON.parse(data);
  const { title, url, price } = data;
  return fetch('http://localhost:4040/api/products', {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: title,
      url,
      price,
    }),
    mode: 'basic',
    credentials: 'include',
  });
}

function removeProduct(data) {
  const id = data.productId;
  return fetch('http://localhost:4040/api/products/' + id, {
    method: 'DELETE',
    mode: 'basic',
    credentials: 'include',
  });
}

ext.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if(request.action === "perform-save") {
    pushProduct(request.data)
      .then(() => {
        console.log("Extension Type: ", "/* @echo extension */");
        console.log("PERFORM AJAX", request.data);

        sendResponse({ action: "saved" });
      })
  } else if (request.action === "perform-remove") {
    removeProduct(request.data)
      .then(() => {
        console.log("Extension Type: ", "/* @echo extension */");
        console.log("PERFORM AJAX", request.data);
        
        sendResponse({ action: "removed" });
      })
  }
});
